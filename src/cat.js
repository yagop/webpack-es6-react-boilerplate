import React from 'react';

export default React.createClass({
  render: function () {
    return <p>{this.props.name} miau!</p>
  },
  propTypes: {
    name: React.PropTypes.string.isRequired
  }
});
