const webpack = require('webpack');

const plugins = [];

if (process.env.NODE_ENV === 'production') {
  plugins.push(
    // This plugin looks for similar chunks and files and merges them
    new webpack.optimize.DedupePlugin(),
    // This plugin minifies all the Javascript code of the final bundle
    new webpack.optimize.UglifyJsPlugin({
      mangle: true,
      compress: {
        warnings: false // Suppress uglification warnings
      }
    }),
    // See https://github.com/facebook/react/issues/6479
    new webpack.DefinePlugin({
      "process.env": {
         NODE_ENV: '"production"'
       }
    })
  );
}

module.exports = {
  entry: ['./src/app.js'],
  output: {
    path: './out',
    filename: 'app.bundle.js'
  },
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    contentBase: 'out',
    inline: true
  },
  module: {
    loaders: [{
      test: /\.jsx?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: {
        presets: ['es2015', 'react']
      }
    }]
  },
  plugins
};
