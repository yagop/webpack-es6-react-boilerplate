## Scripts

```bash
npm run webpack
# Create app.bundle.js into 'out' folder
```

```bash
npm run webpack:production
# Create app.bundle.js into 'out' folder with minified code and deduped
```

```bash
npm run webpack-dev-server
# Starts server listening in http://0.0.0.0:8080
```
